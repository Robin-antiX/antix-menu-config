# antiX menu config

antiX gui frontend script for desktop-menu script, allowing the user to change between different menu styles easily.

| file | usage |
| ------ | ------ |
| desktop-menu | updated backand script ver 3.0 patched for use in antiX 19 and antiX 21 both. |
| antiX-menu-config | GUI frontend for desktop menu. Runs on antiX 19 and antiX 21 both also. |		

## Installation
In order to install the scripts for a test run please perform the following steps:
Best practise is to use an antiX Live (USB/CD/DVD)-Medium for this.

```
sudo mv '/usr/local/bin/desktop-menu' '/usr/local/bin/desktop-menu.bak'
sudp cp '/path/to/downloaded/desktop-menu-test' '/usr/local/bin/desktop-menu'
sudo cp '/path/to/downloaded/antiX-menu-config' '/usr/local/bin/antiX-menu-config'
sudo chown root:root '/usr/local/bin/desktop-menu'
sudo chmod 755 '/usr/local/bin/desktop-menu'
sudo chown root:root '/usr/local/bin/antiX-menu-config'
sudo chmod 755 '/usr/local/bin/antiX-menu-config'
sudo chown root:users /usr/share/desktop-menu/.*/menu-applications
sudo chmod 664 /usr/share/desktop-menu/.*/menu-applications
```

## Usage
After installation on antiX 19.x or antiX 21.x (either i386 or amd_x64) run the command from commandline:
`antiX-menu-config`
In the GUI select from the list what you like best, and press »apply settings« button.
The content of all the submenus of application menu will get changed to your choosen style.
These settings are stored permanently and preset when restarting antiX-menu-config.

## Testing
Check the Menu for the resulting changes. Be aware: Many of the .desktop files present in antiX need to get prepared still for this to work fine, so you will notice some of the entries not changing as expected or displaying strange foreign language descriptions in non English languages. This is not an error of the script, but will be fixed by a .desktop file update once we have finnished.
Repeat with other settings, to find out whether everything works stable and as expected.

To get your original desktop-menu version back in place after checking out the new one simply enter on console:
`sudo mv '/usr/local/bin/desktop-menu.bak' '/usr/local/bin/desktop-menu'`
After this you can set back your menu to the default by entering
`desktop-menu --write-out-global`
None of the other settings from above installation need to get reversed in order to get back to normal.
You will find an additional antX-menu-config subfolder in your `~/.config` folder, keeping the settings file of the GUI dialog.

## Support
Questions, suggestions and bug reporting please to the [antiXforum](https://www.antixforum.com)

## Contributing
Translation resources are present at [antix-contribs at transifex](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/antix-menu-config)

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

## Project status
under construction

-----------
2022, Robin.antiX for antiX community.
